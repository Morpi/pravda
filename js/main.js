$(document).ready(function(){

	// Start OwlCarousel
	$(".owl-carousel").owlCarousel({
		items: 1,
		loop: true,
		autoplay: true,
		autoplayTimeout: 4000,
		autoplayHoverPause: false,
		dots: false,
		smartSpeed: 2000,
		// nav: true,
		// navText: ["<img src='image/arrow.png' class=\"arrow1\">",
		// "<img src='image/arrow.png' class=\"arrow2\">"]
	});

	// Activate WOW.js
	new WOW().init();

	// Add .active for link in header menu
	// target.position().top - Дает позицию всех якорей которые есть на странице
	// в нутри идентификатора #header
	// scroll_top - Дает текущую позицию, на сколько прокручена страница,
	// относительно верха
	function onScroll() {
		var scroll_top = $(document).scrollTop();
		$("#header a").each(function() {
			var hash = $(this).attr("href");
			var target = $(hash);
			if (target.position().top <= scroll_top) {
				$("#header a.active").removeClass("active");
				$(this).addClass("active");
			} else {
				$(this).removeClass("active");
			}
		});
	}

	// Add indent 88px for scrolling anchor(#)
	$(document).ready(onScroll);
	$(document).on("scroll", onScroll);
	$("a").click(function(e) {
		// e.preventDefault();
		$(document).off('scroll');
		$('#header a.active').removeClass('active');
		$(this).addClass('active');

		// var target = this.hash,
		// var target = $(this.hash);
		var hash = $(this).attr('href');
		var target = $(hash);

		$('html, body').stop().animate({
			'scrollTop': target.offset().top - 88
		}, 1500, function () {
			// window.location.hash = hash;
			$(document).on('scroll', onScroll);
		});
	});

	// Очень тупое использование JQuery
	// Три функции для отображения, по нажатию кнопки, меню
	$('#choice1').on('click', function() {
		$('.menu_choice__breakfast.wow.fadeInDown').addClass('active');
		$('.menu_price_breakfast.row.col-md-6.wow.fadeIn.hide').removeClass('hide').addClass('show');

		$('.menu_choice__dinner.wow.fadeInDown.active').removeClass('active');
		$('.menu_choice__supper.wow.fadeInDown.active').removeClass('active');
		$('.menu_price_dinner.row.col-md-6.wow.fadeIn.show').removeClass('show').addClass('hide');
		$('.menu_price_supper.row.col-md-6.wow.fadeIn.show').removeClass('show').addClass('hide');
		$('.wow').css({
			'visibility': 'visible',
			'animation-name': 'fadeIn'
		});
		
	});
	$('#choice2').on('click', function() {
		$('.menu_choice__dinner.wow.fadeInDown').addClass('active');
		$('.menu_price_dinner.row.col-md-6.wow.fadeIn.hide').removeClass('hide').addClass('show');

		$('.menu_choice__breakfast.wow.fadeInDown.active').removeClass('active');
		$('.menu_choice__supper.wow.fadeInDown.active').removeClass('active');
		$('.menu_price_breakfast.row.col-md-6.wow.fadeIn.show').removeClass('show').addClass('hide');
		$('.menu_price_supper.row.col-md-6.wow.fadeIn.show').removeClass('show').addClass('hide');
		$('.wow').css({
			'visibility': 'visible',
			'animation-name': 'fadeIn'
		});
		
	});
	$('#choice3').on('click', function() {
		$('.menu_choice__supper.wow.fadeInDown').addClass('active');
		$('.menu_price_supper.row.col-md-6.wow.fadeIn.hide').removeClass('hide').addClass('show');

		$('.menu_choice__breakfast.wow.fadeInDown.active').removeClass('active');
		$('.menu_choice__dinner.wow.fadeInDown.active').removeClass('active');
		$('.menu_price_breakfast.row.col-md-6.wow.fadeIn.show').removeClass('show').addClass('hide');
		$('.menu_price_dinner.row.col-md-6.wow.fadeIn.show').removeClass('show').addClass('hide');
		$('.wow').css({
			'visibility': 'visible',
			'animation-name': 'fadeIn'
		});
	});

});